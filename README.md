# pars-package-manager
Pars tema yöneticisi

# Kullanım şekli


pars [kur,uygula,sil,listele,indir,depo,bilgi] [dosya yolu,tema ismi,depo adresi] 

Açıklamalar:

-k kur                    : Pars uzantılı temayı kurar.

-u uygula                 : Kurulu temayı uygular

-s sil                    : Kurulu temayı kaldırır.

-l listele                : Kurulu temaları listeler.

-p paketle                : Tema paketler.

-t temizle                : Temanın dosyalarını temizler.

-d depo                   : Pars deposu ayarlar.

-i indir                  : Paket indirir.

\+                         : Parsı günceller.

dosya yolu                : Kurulacak temanın adresidir.

tema ismi                 : Uygulanacak temanın ismi.

indir                     : Paketi depodan indirir.

depo                      : Depoyu ayarlar.


# Paket iç yapısı

1-kur                     : Kurulum betiği. Kurulum esnasında çalıştırılır.

2-kaldir                  : Kaldırma betiği Kaldırma esnasında çalıştırılır.

3-uygula                  : Temayı uygulamak için çalıştırılan betik.

4-isim                    : Paketin isminin yazılı olduğu dosya.

5-ekler                   : Paketin kurulması kaldırılması uygulanması esnasında
                            lazım olabilecek dosyalar.


Not: paket kurulurken ve kaldırılırken dosyalar sisteme kopyalanmaz betikler ile elle kopyalanması gerekir.


# Bu reposity depo özelliği taşır depoyu tanımlamak için:

pars depo https://gitlab.com/parduscix/pars-theme-manager/raw/master

# Kurulum için:

wget https://gitlab.com/parduscix/pars-theme-manager/raw/master/pars pars ; mv -f pars /usr/bin/pars ; chmod +x /usr/bin/pars
